//
//  SimpleHookVC.swift
//  ARTest
//
//  Created by Vladimir Azov on 22/05/2018.
//  Copyright © 2018 Vladimir Azov. All rights reserved.
//

import UIKit
import ARKit

class SimpleHookVC : UIViewController, ARSCNViewDelegate, SCNPhysicsContactDelegate {
    
    var player: Player = Player()
    var hookBind: HookBind = HookBind()
    static private let fireDistance: CGFloat = 10
    static private let hookLength: CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneView.delegate = self
        sceneView.scene.physicsWorld.contactDelegate = self
        let configuration = ARWorldTrackingConfiguration()
        //configuration.planeDetection = [.horizontal, .vertical]
        sceneView.session.run(configuration)
        
        hookBind.position = SCNVector3(0, 0, -1)
        player.position = SCNVector3(-2, 0.5, -3)
        
//        let deadlineTime = DispatchTime.now() + .seconds(2)
//        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
//            SCNTransaction.begin()
//            SCNTransaction.animationDuration = 5
//            hookBind.pivot = SCNMatrix4MakeTranslation(0, -(hookBind.height/2), 0) // new height
//            SCNTransaction.commit()
//        }
        
        sceneView.scene.rootNode.addChildNode(hookBind)
        sceneView.scene.rootNode.addChildNode(player)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBOutlet var sceneView: ARSCNView!
    
    //MARK: actions
    @IBAction func shootTouched(_ sender: UIButton) {
        let aim = Aim()
        let (direction, position) = directionVector
        aim.position = position
        sceneView.scene.rootNode.addChildNode(aim)
        let fire = SCNAction.moveBy(x: CGFloat(direction.x) * SimpleHookVC.fireDistance,
                                    y: CGFloat(direction.y) * SimpleHookVC.fireDistance,
                                    z: CGFloat(direction.z) * SimpleHookVC.fireDistance,
                                    duration: 1)
        fire.speed = 3
        //fire.timingMode = .linear
        aim.runAction(fire)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            aim.removeFromParentNode()
        }
    }
    
    // MARK: - private
    fileprivate var directionVector: (SCNVector3, SCNVector3) { // (direction, position)
        if let frame = self.sceneView.session.currentFrame {
            let mat = SCNMatrix4.init(frame.camera.transform) // 4x4 transform matrix describing camera in world space
            let dir = SCNVector3(-1 * mat.m31, -1 * mat.m32, -1 * mat.m33) // orientation of camera in world space
            let pos = SCNVector3(mat.m41, mat.m42, mat.m43) // location of camera in world space
            
            return (dir, pos)
        }
        return (SCNVector3(0, 0, 0), SCNVector3(0, 0, 0))
    }
    
    private func directionVector(transform: matrix_float4x4) -> (SCNVector3, SCNVector3) {
        let mat = SCNMatrix4.init(transform) // 4x4 transform matrix describing camera in world space
        let dir = SCNVector3(-1 * mat.m31, -1 * mat.m32, -1 * mat.m33) // orientation of camera in world space
        let pos = SCNVector3(mat.m41, mat.m42, mat.m43) // location of camera in world space
        
        return (dir, pos)
    }
    
    private func shoot(from: SCNVector3, to: SCNVector3) {
        let hook = Hook()
        hook.position = from
        sceneView.scene.rootNode.addChildNode(hook)
        let length: Float = self.distanceBetween(from: from, to: to)
        let vectorTo: SCNVector3 = SCNVector3Make(to.x/length, to.y/length, to.z/length)
        let fire = SCNAction.move(to: vectorTo, duration: 1)
        fire.timingMode = .easeInEaseOut
        hook.runAction(fire)
        //print(distanceBetween(from: from, to: to))
        //print(distanceBetween(from: from, to: vectorTo))
    }
    
    private func distanceBetween(from: SCNVector3, to: SCNVector3) -> Float {
        let node1Pos = from
        let node2Pos = to
        let distance = SCNVector3(
            node2Pos.x - node1Pos.x,
            node2Pos.y - node1Pos.y,
            node2Pos.z - node1Pos.z
        )
        return sqrtf(distance.x * distance.x + distance.y * distance.y + distance.z * distance.z)
    }
    
    //MARK: - SCNPhysicsContactDelegate
    func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {
        if contact.nodeA is Aim {
            contact.nodeA.removeFromParentNode()
        }
        else if contact.nodeB is Aim {
            contact.nodeB.removeFromParentNode()
        }
        if !(contact.nodeA is Aim) && !(contact.nodeB is Aim) {
            return
        }
        let direction = SCNVector3(contact.contactPoint.x, contact.contactPoint.y, contact.contactPoint.z)
        self.shoot(from: player.position, to: direction)
    }
}


//    func buildLineInTwoPointsWithRotation(from startPoint: SCNVector3,
//                                          to endPoint: SCNVector3,
//                                          radius: CGFloat,
//                                          color: UIColor) -> SCNNode {
//        let node: SCNNode = SCNNode()
//
//        let w = SCNVector3(x: endPoint.x-startPoint.x,
//                           y: endPoint.y-startPoint.y,
//                           z: endPoint.z-startPoint.z)
//        let l = CGFloat(sqrt(w.x * w.x + w.y * w.y + w.z * w.z))
//
//        if l == 0.0 {
//            // two points together.
//            let sphere = SCNSphere(radius: radius)
//            sphere.firstMaterial?.diffuse.contents = color
//            node.geometry = sphere
//            node.position = startPoint
//            return node
//
//        }
//
//        let cyl = SCNCylinder(radius: radius, height: l)
//        cyl.firstMaterial?.diffuse.contents = color
//
//        node.geometry = cyl
//
//        //original vector of cylinder above 0,0,0
//        let ov = SCNVector3(0, l/2.0,0)
//        //target vector, in new coordination
//        let nv = SCNVector3((endPoint.x - startPoint.x)/2.0, (endPoint.y - startPoint.y)/2.0,
//                            (endPoint.z-startPoint.z)/2.0)
//
//        // axis between two vector
//        let av = SCNVector3( (ov.x + nv.x)/2.0, (ov.y+nv.y)/2.0, (ov.z+nv.z)/2.0)
//
//        //normalized axis vector
//        let av_normalized = normalizeVector(av)
//        let q0 = Float(0.0) //cos(angel/2), angle is always 180 or M_PI
//        let q1 = Float(av_normalized.x) // x' * sin(angle/2)
//        let q2 = Float(av_normalized.y) // y' * sin(angle/2)
//        let q3 = Float(av_normalized.z) // z' * sin(angle/2)
//
//        let r_m11 = q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3
//        let r_m12 = 2 * q1 * q2 + 2 * q0 * q3
//        let r_m13 = 2 * q1 * q3 - 2 * q0 * q2
//        let r_m21 = 2 * q1 * q2 - 2 * q0 * q3
//        let r_m22 = q0 * q0 - q1 * q1 + q2 * q2 - q3 * q3
//        let r_m23 = 2 * q2 * q3 + 2 * q0 * q1
//        let r_m31 = 2 * q1 * q3 + 2 * q0 * q2
//        let r_m32 = 2 * q2 * q3 - 2 * q0 * q1
//        let r_m33 = q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3
//
//        node.transform.m11 = r_m11
//        node.transform.m12 = r_m12
//        node.transform.m13 = r_m13
//        node.transform.m14 = 0.0
//
//        node.transform.m21 = r_m21
//        node.transform.m22 = r_m22
//        node.transform.m23 = r_m23
//        node.transform.m24 = 0.0
//
//        node.transform.m31 = r_m31
//        node.transform.m32 = r_m32
//        node.transform.m33 = r_m33
//        node.transform.m34 = 0.0
//
//        node.transform.m41 = (startPoint.x + endPoint.x) / 2.0
//        node.transform.m42 = (startPoint.y + endPoint.y) / 2.0
//        node.transform.m43 = (startPoint.z + endPoint.z) / 2.0
//        node.transform.m44 = 1.0
//
//        return node
//    }
//
//    func normalizeVector(_ iv: SCNVector3) -> SCNVector3 {
//        let length = sqrt(iv.x * iv.x + iv.y * iv.y + iv.z * iv.z)
//        if length == 0 {
//            return SCNVector3(0.0, 0.0, 0.0)
//        }
//
//        return SCNVector3( iv.x / length, iv.y / length, iv.z / length)
//
//    }

