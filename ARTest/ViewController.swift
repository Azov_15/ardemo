//
//  ViewController.swift
//  ARTest
//
//  Created by Vladimir Azov on 16/05/2018.
//  Copyright © 2018 Vladimir Azov. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    private let metalDevice: MTLDevice? = MTLCreateSystemDefaultDevice()
    private var currPlaneId: Int = 0
    var player: Player = Player()
    var planes: Array = [SCNNode]()
    var hookBinds: Array = [HookBind]()
    
    static private let fireDistance: CGFloat = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        //sceneView.showsStatistics = true
        sceneView.scene.physicsWorld.contactDelegate = self
        // Create a new scene
        //let scene = SCNScene(named: "art.scnassets/ship.scn")!
//        sceneView.debugOptions = [
//            ARSCNDebugOptions.showFeaturePoints,
//            ARSCNDebugOptions.showWorldOrigin
//        ]
        
        player.position = SCNVector3(0, 0, 0)
        sceneView.scene.rootNode.addChildNode(player)
        // Set the scene to the view
        //sceneView.scene = scene
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.createTrackingConfig()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    // MARK: - IBAction
    
    @IBAction func startStopTrackingTouched(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if (sender.isSelected) {
            let configuration :ARWorldTrackingConfiguration = self.sceneView.session.configuration as! ARWorldTrackingConfiguration
            configuration.planeDetection = []
            self.sceneView.session.run(configuration)
        }
        else {
            self.createTrackingConfig()
        }
    }
    
    @IBAction func addBoxTouched(_ sender: Any) {
        self.addBoxes()
    }
    
    @IBAction func shootTouched(_ sender: Any) {
        let aim = Aim()
        let (direction, position) = cameraVector
        aim.position = position
        sceneView.scene.rootNode.addChildNode(aim)
        let fire = SCNAction.moveBy(x: CGFloat(direction.x) * ViewController.fireDistance,
                                    y: CGFloat(direction.y) * ViewController.fireDistance,
                                    z: CGFloat(direction.z) * ViewController.fireDistance,
                                    duration: 1)
        //fire.speed = 1
        //fire.timingMode = .linear
        aim.runAction(fire)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            aim.removeFromParentNode()
        }
    }
    
    // MARK: - private
    fileprivate var cameraVector: (SCNVector3, SCNVector3) { // (direction, position)
        if let frame = self.sceneView.session.currentFrame {
            let mat = SCNMatrix4.init(frame.camera.transform) // 4x4 transform matrix describing camera in world space
            let dir = SCNVector3(-1 * mat.m31, -1 * mat.m32, -1 * mat.m33) // orientation of camera in world space
            let pos = SCNVector3(mat.m41, mat.m42, mat.m43) // location of camera in world space
            
            return (dir, pos)
        }
        return (SCNVector3(0, 0, 0), SCNVector3(0, 0, 0))
    }
    
    func createTrackingConfig() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal, .vertical]
        sceneView.session.run(configuration)
    }
    
    func createPlaneNode(planeAnchor: ARPlaneAnchor) -> SCNNode {
        let scenePlaneGeometry = ARSCNPlaneGeometry(device: metalDevice!)
        scenePlaneGeometry?.update(from: planeAnchor.geometry)
        let planeNode = SCNNode(geometry: scenePlaneGeometry)
        planeNode.name = "\(currPlaneId)"
        planeNode.opacity = 0.25
        if planeAnchor.alignment == .horizontal {
            planeNode.geometry?.firstMaterial?.diffuse.contents = UIColor.blue
        } else {
            planeNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
        }
        
        planeNode.physicsBody?.categoryBitMask = CollisionCategory.arBullets.rawValue
        planeNode.physicsBody?.contactTestBitMask = CollisionCategory.logos.rawValue
        
        currPlaneId += 1
        return planeNode
    }
    
    func addBoxes() {
        for plane in planes {
            let hookBind = HookBind()
            
            hookBind.position = SCNVector3Make(0, 0.5, 0)
            self.hookBinds.append(hookBind)
            plane.addChildNode(hookBind)
        }
    }
    
    private func shoot(from: SCNVector3, to: SCNVector3) {
        let hook = Hook()
        hook.position = from
        
        sceneView.scene.rootNode.addChildNode(hook)
        //let length: Float = self.distanceBetween(from: from, to: to)
        //let vectorTo: SCNVector3 = SCNVector3Make(to.x/length, to.y/length, to.z/length)
        let fire = SCNAction.move(to: to, duration: 1)
        fire.timingMode = .easeInEaseOut
        hook.runAction(fire)
    }
    
    private func movePlayer(to: SCNVector3) {
        let fire = SCNAction.move(to: to, duration: 1)
        fire.timingMode = .easeInEaseOut
        player.runAction(fire)
    }
    
    private func distanceBetween(from: SCNVector3, to: SCNVector3) -> Float {
        let node1Pos = from
        let node2Pos = to
        let distance = SCNVector3(
            node2Pos.x - node1Pos.x,
            node2Pos.y - node1Pos.y,
            node2Pos.z - node1Pos.z
        )
        return sqrtf(distance.x * distance.x + distance.y * distance.y + distance.z * distance.z)
    }
    
    // MARK: - ARSCNViewDelegate
    

    // Override to create and configure nodes for anchors added to the view's session.
//    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
//        let node = SCNNode()
//
//        return node
//    }

    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        // only care about detected planes (i.e. `ARPlaneAnchor`s)
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        node.enumerateChildNodes { (childNode, _) in
            if planes.count > 0 {
                for i in 0...planes.count-1 {
                    if childNode == planes[i] {
                        planes.remove(at: i)
                        break
                    }
                }
            }
            childNode.removeFromParentNode()
        }
        let planeNode = createPlaneNode(planeAnchor: planeAnchor)
        node.addChildNode(planeNode)
        planes.append(planeNode)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        guard let _ = anchor as? ARPlaneAnchor else { return }
        node.enumerateChildNodes { (childNode, _) in
            if planes.count > 0 {
                for i in 0...planes.count-1 {
                    if childNode == planes[i] {
                        planes.remove(at: i)
                        break
                    }
                }
            }
            childNode.removeFromParentNode()
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        let planeNode = createPlaneNode(planeAnchor: planeAnchor)
        node.addChildNode(planeNode)
        planes.append(planeNode)
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}

extension ViewController: SCNPhysicsContactDelegate {
    
    func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {
        if contact.nodeA is Aim {
            contact.nodeA.removeFromParentNode()
        }
        else if contact.nodeB is Aim {
            contact.nodeB.removeFromParentNode()
        }
//        if !(contact.nodeA is Aim) && !(contact.nodeB is Aim) {
//            return
//        }
        let direction = SCNVector3(contact.contactPoint.x, contact.contactPoint.y, contact.contactPoint.z)
        if contact.nodeA is HookBind && contact.nodeB is Hook {
            contact.nodeB.removeFromParentNode()
            self.movePlayer(to: direction)
        }
        else {
            self.shoot(from: player.position, to: direction)
        }
//        guard let nodeABitMask = contact.nodeA.physicsBody?.categoryBitMask,
//            let nodeBBitMask = contact.nodeB.physicsBody?.categoryBitMask,
//            nodeABitMask & nodeBBitMask == CollisionCategory.logos.rawValue & CollisionCategory.arBullets.rawValue else {
//                return
//        }
//
//        contact.nodeB.removeFromParentNode()
//        logoCount -= 1
//
//        if logoCount == 0 {
//            DispatchQueue.main.async {
//                self.stopGame()
//            }
//        }
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
//            contact.nodeA.removeFromParentNode()
//        })
    }
    
}
