//
//  Player.swift
//  ARTest
//
//  Created by Vladimir Azov on 22/05/2018.
//  Copyright © 2018 Vladimir Azov. All rights reserved.
//

import Foundation
import ARKit

final class Player: SCNNode {
    
    private static let boxSide: CGFloat = 0.1
    
    override init() {
        super.init()
        initialization()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialization()
    }
    
    private func initialization() {
        let box = SCNBox(width: Player.boxSide, height: Player.boxSide, length: Player.boxSide, chamferRadius: 0)
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.green
        box.materials = [material]
        let boxNode = SCNNode(geometry: box)
        self.geometry = box
        //self.addChildNode(boxNode)
//        let shape = SCNPhysicsShape(geometry: box, options: nil)
//
//        self.physicsBody = SCNPhysicsBody(type: .static, shape: shape)
//        self.physicsBody?.isAffectedByGravity = false
//
//        self.physicsBody?.categoryBitMask = CollisionCategory.logos.rawValue
//        self.physicsBody?.contactTestBitMask = CollisionCategory.arBullets.rawValue
    }
}
