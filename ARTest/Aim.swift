//
//  Aim.swift
//  ARTest
//
//  Created by Vladimir Azov on 23/05/2018.
//  Copyright © 2018 Vladimir Azov. All rights reserved.
//

import Foundation
import ARKit

final class Aim: SCNNode {
    
    private let sphereRadius: CGFloat = 0.01
    
    override init() {
        super.init()
        initialization()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialization()
    }
    
    private func initialization() {
        let aim = SCNSphere(radius: sphereRadius)
        self.geometry = aim
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.black
        aim.materials = [material]
        let shape = SCNPhysicsShape(geometry: aim, options: nil)
        self.physicsBody = SCNPhysicsBody(type: .dynamic, shape: shape)
        self.physicsBody?.isAffectedByGravity = false
        
        self.physicsBody?.categoryBitMask = CollisionCategory.arBullets.rawValue
        self.physicsBody?.contactTestBitMask = CollisionCategory.logos.rawValue
    }
}
