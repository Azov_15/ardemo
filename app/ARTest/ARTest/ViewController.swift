//
//  ViewController.swift
//  ARTest
//
//  Created by Evgeny Serikov on 13.06.2018.
//  Copyright © 2018 EMC. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController {

    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var controlPanel:ControlPanel!
    @IBOutlet weak var counter:UILabel!
    var counterValue:Int = 0
    
    var planeNodes = [PlaneNode]()
    var coins = [Coin]()
    var world:World?
    
    var character:Character?
    var direction:float2? = nil
    var jump:Float = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapGestureToSceneView()
        configureLighting()
        self.controlPanel.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpSceneView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    func setUpSceneView() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        
        sceneView.session.run(configuration)
        sceneView.delegate = self
        sceneView.scene.physicsWorld.contactDelegate = self
        controlPanel.session = sceneView.session
    }
    
    func configureLighting() {
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
    }
    
    func addTapGestureToSceneView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.addRocketshipToSceneView(withGestureRecognizer:)))
        sceneView.addGestureRecognizer(tapGestureRecognizer)
    }

    @objc func addRocketshipToSceneView(withGestureRecognizer recognizer: UIGestureRecognizer) {
        let tapLocation = recognizer.location(in: sceneView)
        let hitTestResults = sceneView.hitTest(tapLocation, types: .existingPlaneUsingExtent)
        guard let hitTestResult = hitTestResults.first else { return }
        
        let translation = hitTestResult.worldTransform.translation
        let x = translation.x
        let y = translation.y
        let z = translation.z
        
        if self.controlPanel.segmentedControl.selectedSegmentIndex == 0 {
            if self.character == nil {
                self.character = Character(position: SCNVector3(x,y+0.3,z))
                sceneView.scene.rootNode.addChildNode(self.character!.node)
            } else {
                self.character?.node.position = SCNVector3(x,y+0.3,z)
            }
        } else if self.controlPanel.segmentedControl.selectedSegmentIndex == 1 {
            let coin = Coin(position: SCNVector3(x,y,z))
            sceneView.scene.rootNode.addChildNode(coin.node)
        } else if self.controlPanel.segmentedControl.selectedSegmentIndex == 2 {
            if(self.world  == nil) {
                world = World(position: SCNVector3(x,y,z))
                sceneView.scene.rootNode.addChildNode(world!.rootNode)
                for node in planeNodes {
                    node.geometry?.firstMaterial?.diffuse.contents = UIColor.clear
                }
            }
        }
    }
    
    // MARK: - ARSCNViewDelegate
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        // Do something with the new transform
    }
}

extension ViewController: ControlPanelDelegate {
    func jump(panel: ControlPanel) {
        if self.jump <= 0.0 {
            self.jump = 2.3
        }
    }
    
    func walk(panel: ControlPanel, direction: float2?) {
        self.direction = direction
    }
}

extension ViewController:SCNPhysicsContactDelegate
{
    func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {
        
        DispatchQueue.main.async {
            self.counterValue += 1
            self.counter.text = "\(self.counterValue)"
        }
        
        if contact.nodeA == self.character?.node {
            contact.nodeB.removeFromParentNode()
        } else {
            contact.nodeA.removeFromParentNode()
        }
    }
}

extension ViewController: ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if(self.world == nil) {
            guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
            let planeNode = PlaneNode(plane: planeAnchor)
            node.addChildNode(planeNode)
            planeNodes.append(planeNode)
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        if(self.world == nil) {
            guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
            node.enumerateChildNodes { (childNode, _) in
                if planeNodes.count > 0 {
                    for i in 0...planeNodes.count-1 {
                        if childNode == planeNodes[i] {
                            planeNodes.remove(at: i)
                            break
                        }
                    }
                }
                childNode.removeFromParentNode()
            }
            let planeNode = PlaneNode(plane: planeAnchor)
            node.addChildNode(planeNode)
            planeNodes.append(planeNode)
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        //print("didRemove", node.name ?? "")
        if(self.world == nil) {
            guard let _ = anchor as? ARPlaneAnchor else { return }
            node.enumerateChildNodes { (childNode, _) in
                if planeNodes.count > 0 {
                    for i in 0...planeNodes.count-1 {
                        if childNode == planeNodes[i] {
                            planeNodes.remove(at: i)
                            break
                        }
                    }
                }
                childNode.removeFromParentNode()
            }
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        if(jump > 0.0) {
            jump = max(0, jump-0.05)
        }
        if let direction = direction {
            let d = float3(0.25*direction.x, jump, 0.25*direction.y)
            character?.walkInDirection(d, time: time, scene: self.sceneView.scene)
        } else {
            let d = float3(0, jump, 0)
          character?.walkInDirection(d, time: time, scene: self.sceneView.scene)
        }
    }
}

extension float4x4 {
    var translation: float3 {
        let translation = self.columns.3
        return float3(translation.x, translation.y, translation.z)
    }
}

extension UIColor {
    open class var transparentWhite: UIColor {
        return UIColor.white.withAlphaComponent(0.20)
    }
}
