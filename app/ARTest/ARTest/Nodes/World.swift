//
//  World.swift
//  ARTest
//
//  Created by Evgeny Serikov on 20.06.2018.
//  Copyright © 2018 EMC. All rights reserved.
//

import SceneKit

class World: NSObject {
    
    var rootNode:SCNNode!
    
    init(position:SCNVector3) {
        let scene = SCNScene(named: "art.scnassets/level1.scn")!
        
        scene.rootNode.position = position
        self.rootNode = scene.rootNode

    }

}
