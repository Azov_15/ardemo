//
//  Character.swift
//  ARTest
//
//  Created by Evgeny Serikov on 14.06.2018.
//  Copyright © 2018 EMC. All rights reserved.
//

import SceneKit
import ARKit

class Character {

    var node:SCNNode!
    
    private var walkAnimation: CAAnimation!
    static let speedFactor = Float(1.538)
    private var previousUpdateTime = TimeInterval(0.0)
    
    private var directionAngle: SCNFloat = 0.0 {
        didSet {
            if directionAngle != oldValue {
                node.runAction(SCNAction.rotateTo(x: 0.0, y: CGFloat(directionAngle), z: 0.0, duration: 0.1, usesShortestUnitArc: true))
            }
        }
    }
    
    init(position:SCNVector3) {
        guard let rocketshipScene = SCNScene(named: "art.scnassets/panda.scn"),
            let characterNode = rocketshipScene.rootNode.childNode(withName: "panda", recursively: false)
            else { return }
        
        characterNode.position = position
        
        // TODO: Attach physics body to rocketship node
        let nodeScale = SCNVector3(0.3,0.3,0.3)
        let physicsBody = SCNPhysicsBody(type: .kinematic, shape:SCNPhysicsShape(node: characterNode, options: [SCNPhysicsShape.Option.scale: nodeScale]))
        physicsBody.contactTestBitMask = 1
        characterNode.physicsBody = physicsBody
        characterNode.name = "panda"
        self.node = characterNode
        
        walkAnimation = CAAnimation.animationWithSceneNamed("art.scnassets/walk.scn")
        walkAnimation.usesSceneTimeBase = false
        walkAnimation.fadeInDuration = 0.3
        walkAnimation.fadeOutDuration = 0.3
        walkAnimation.repeatCount = Float.infinity
        walkAnimation.speed = Character.speedFactor
    }
    
    private var isWalking: Bool = false {
        didSet {
            if oldValue != isWalking {
                // Update node animation.
                if isWalking {
                    node.addAnimation(walkAnimation, forKey: "walk")
                } else {
                    node.removeAnimation(forKey: "walk", blendOutDuration: 0.2)
                }
            }
        }
    }
    
    
    func jump(ditection:SCNVector3, strength:CGFloat) {
        
        let jumpField = SCNPhysicsField.linearGravity()
        jumpField.direction = ditection
        jumpField.strength = strength
        self.node.physicsField = jumpField
        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
            self.node.physicsField = nil
        }
    }

    private var accelerationY = SCNFloat(0.0) // Simulate gravity
    
    func walkInDirection(_ direction: float3, time: TimeInterval, scene: SCNScene) {
        
        // delta time since last update
        if previousUpdateTime == 0.0 {
            previousUpdateTime = time
        }
        
        let deltaTime:Float = Float(min(time - previousUpdateTime, 1.0 / 60.0))
        let characterSpeed = deltaTime * Character.speedFactor * 0.84
        previousUpdateTime = time
        
        let initialPosition = node.position
        
        // move
        if direction.x != 0.0 || direction.y != 0.0 || direction.z != 0.0 {
            // move character
            let position = float3(node.position)
            node.position = SCNVector3(position + direction * characterSpeed)
            
            // update orientation
            directionAngle = SCNFloat(atan2(direction.x, direction.z))
            
            isWalking = true
        }
        else {
            isWalking = false
        }

        
        // Update the altitude of the character
        
        var position = node.position
        var p0 = position
        var p1 = position
        
        let maxRise = SCNFloat(0.08)
        let maxJump = SCNFloat(10.0)
        p0.y -= maxJump
        p1.y += maxRise
        
        // Do a vertical ray intersection

        let results = scene.physicsWorld.rayTestWithSegment(from: p1, to: p0, options:[ .searchMode: SCNPhysicsWorld.TestSearchMode.all])
        
        if results.count>0 {
            var groundAltitude = results.first?.worldCoordinates.y ?? 0
            for result in results {
                groundAltitude = max(groundAltitude, result.worldCoordinates.y)
            }

            let threshold = SCNFloat(1e-5)
            let gravityAcceleration = SCNFloat(0.08)
            
            if groundAltitude < position.y - threshold {
                accelerationY += SCNFloat(deltaTime) * gravityAcceleration // approximation of acceleration for a delta time.

            }
            else {
                accelerationY = 0
            }
            
            position.y -= accelerationY
            
            // reset acceleration if we touch the ground
            if groundAltitude > position.y {
                accelerationY = 0
                position.y = groundAltitude
            }
            
            // Finally, update the position of the character.
            node.position = position
            
        }
 
    }
}



extension CAAnimation {
    class func animationWithSceneNamed(_ name: String) -> CAAnimation? {
        var animation: CAAnimation?
        if let scene = SCNScene(named: name) {
            scene.rootNode.enumerateChildNodes({ (child, stop) in
                if child.animationKeys.count > 0 {
                    animation = child.animation(forKey: child.animationKeys.first!)
                    stop.initialize(to: true)
                }
            })
        }
        return animation
    }
}

extension SCNTransaction {
    class func animateWithDuration(_ duration: CFTimeInterval = 0.25, timingFunction: CAMediaTimingFunction? = nil, completionBlock: (() -> Void)? = nil, animations: () -> Void) {
        begin()
        self.animationDuration = duration
        self.completionBlock = completionBlock
        self.animationTimingFunction = timingFunction
        animations()
        commit()
    }
}
