//
//  PlaneNode.swift
//  ARTest
//
//  Created by Evgeny Serikov on 13.06.2018.
//  Copyright © 2018 EMC. All rights reserved.
//

import SceneKit
import ARKit

class PlaneNode: SCNNode {
    init(plane:ARPlaneAnchor) {
        super.init()
        
        let scenePlaneGeometry = ARSCNPlaneGeometry(device: MTLCreateSystemDefaultDevice()!)
        scenePlaneGeometry?.update(from: plane.geometry)
       
        self.geometry = scenePlaneGeometry
        
        let material = SCNMaterial()
        material.transparency = 0.25
        self.geometry?.materials = [material]
        if plane.alignment == .horizontal {
            self.geometry?.firstMaterial?.diffuse.contents = UIColor.blue
        } else {
            self.geometry?.firstMaterial?.diffuse.contents = UIColor.red
        }
        
        self.update(type: .static, arPlane: plane.geometry)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update( type: SCNPhysicsBodyType, arPlane: ARPlaneGeometry) {
        var indices: [Int32] = [Int32(arPlane.vertices.count)]
        
        var vertices: [SCNVector3] = [SCNVector3]()
        
        var i = 0
        for vertex in arPlane.vertices {
            vertices.append(SCNVector3Make(vertex.x, vertex.y, vertex.z))
            indices.append(Int32(i))
            i += 1
        }
        
        let vertexSource = SCNGeometrySource(vertices: vertices)
        let indexData = Data(bytes: indices, count: indices.count * MemoryLayout<Int32>.size)
        let element = SCNGeometryElement(data: indexData, primitiveType: .polygon, primitiveCount: 1, bytesPerIndex: MemoryLayout<Int32>.size)
        let geometry = SCNGeometry(sources: [vertexSource], elements: [element])
        
        let shape = SCNPhysicsShape(geometry: geometry, options: nil)
        
        let physicsBody = SCNPhysicsBody(type: type, shape: shape)
        self.physicsBody = physicsBody
    }
}
