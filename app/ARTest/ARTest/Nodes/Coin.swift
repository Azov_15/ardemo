//
//  Coin.swift
//  ARTest
//
//  Created by Evgeny Serikov on 14.06.2018.
//  Copyright © 2018 EMC. All rights reserved.
//

import SceneKit

class Coin {

    var node:SCNNode!
    
    init(position:SCNVector3) {
        
        self.node = SCNNode(geometry: SCNCylinder(radius: 0.01, height: 0.01))
        
        let material = SCNMaterial()
        material.transparency = 0.75
        self.node.geometry?.materials = [material]
        self.node.geometry?.firstMaterial?.diffuse.contents = UIColor.yellow
        self.node.position = position
        let nodeScale = SCNVector3(0.5,0.5,0.5)
        let physicsBody = SCNPhysicsBody(type: .kinematic, shape:SCNPhysicsShape(node: self.node, options: [SCNPhysicsShape.Option.scale: nodeScale]))
        physicsBody.collisionBitMask = 1
        self.node.physicsBody = physicsBody

    }
    
}
