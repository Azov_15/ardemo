//
//  ControlPanel.swift
//  ARTest
//
//  Created by Evgeny Serikov on 14.06.2018.
//  Copyright © 2018 EMC. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

protocol ControlPanelDelegate: class {
    func jump(panel:ControlPanel)
    func walk(panel:ControlPanel, direction:float2?)

}

class ControlPanel: UIView {

    weak var delegate:ControlPanelDelegate? = nil
    
    var session: ARSession?

    @IBOutlet weak var jump: UIButton!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationViewCenter: UIView!
    @IBOutlet var navigationViewAnctor: UIView!
    @IBOutlet var navigationViewAnctorX: NSLayoutConstraint!
    @IBOutlet var navigationViewAnctorY: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.navigationViewAnctor.isHidden = true
        self.navigationViewAnctor.layer.cornerRadius = navigationViewAnctor.bounds.size.width/2.0
        
        self.navigationView.layer.cornerRadius = self.navigationView.bounds.size.width/2.0
        self.navigationView.layer.borderWidth = 2.0
        self.navigationView.layer.borderColor = UIColor.white.cgColor
        navigationViewCenter.layer.cornerRadius = navigationViewCenter.bounds.size.width/2.0
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @objc @IBAction func jumpAction(sender: UIButton) {
        self.delegate?.jump(panel: self)
    }
    
    @objc @IBAction func addShipToSceneView(withGestureRecognizer recognizer: UIGestureRecognizer) {
        let tapLocation = recognizer.location(in: self.navigationView)
        
        if recognizer.state == .ended {
            self.delegate?.walk(panel: self, direction: nil)
          //  ditection = nil
            self.navigationViewAnctor.isHidden = true
        } else {
            self.navigationViewAnctor.isHidden = false
            let radius = navigationView.bounds.size.height/2.0
            let v1 = CGPoint(x: radius, y: 0)
            let v2 = CGPoint(x: tapLocation.x-radius, y: tapLocation.y-radius)
            
            let ab = v1.x*v2.x+v1.y*v2.y
            let a = sqrt(v1.x*v1.x + v1.y*v1.y)
            let b = sqrt(v2.x*v2.x + v2.y*v2.y)
            var angle = acos(ab/(a*b))//+CGFloat(Double.pi)
            
            let x = radius + radius*cos(angle)
            let y = radius + radius*sin(angle)*((v2.y<0) ? -1.0 : 1.0)
            
            navigationViewAnctorX.constant = x-navigationViewAnctor.bounds.size.width/2.0
            navigationViewAnctorY.constant = y-navigationViewAnctor.bounds.size.height/2.0
            angle = angle * ((v2.y<0) ? 1.0 : -1.0)
            angle += CGFloat(Float.pi/2.0)
            angle += CGFloat(session?.currentFrame?.camera.eulerAngles.y ?? 0.0)

            self.delegate?.walk(panel: self, direction: float2(Float(sin(angle)),
                                                               Float(cos(angle))))

        }
        
    }
    
}
